
export async function fetchFirstTodo() {
	const response = fetch('https://jsonplaceholder.typicode.com/todos/1')
		.then(response => response.json())
	return response
}