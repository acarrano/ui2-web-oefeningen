import * as toTitleCase from 'to-title-case'

export function greeting () {
  console.log('Hello from module myfunctions')
}

export function convertToTitleCase (title) {
  return toTitleCase(title)
}
