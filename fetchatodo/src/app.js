import * as funcs from './js/myfunctions'
import './css/style.css'
import * as rest from './js/resclient'

window.addEventListener('load', run)

function run() {
	const data = rest.fetchFirstTodo()
	console.log(data)
	data.then(json => {
		document.getElementsByTagName('input')[0].value = json.id
		document.getElementsByTagName('input')[1].value = json.title
		document.getElementsByTagName('input')[2].checked = json.completed
		}
	)
}