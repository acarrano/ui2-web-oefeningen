const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const dist = path.resolve(__dirname, 'dist')

module.exports = {
  devtool: 'source-map',
  mode: 'development',
  entry: './src/app.js',
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(eot|woff(2)?|ttf|otf|svg)$/i,
        type: 'asset'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({ template: path.resolve(__dirname, 'src/html/index.html') })
  ],
  devServer: {
    contentBase: dist,
    open: true,
    compress: true,
    overlay: true
  }
}
