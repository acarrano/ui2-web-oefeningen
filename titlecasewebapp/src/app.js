import * as funcs from './js/myfunctions'
import './css/style.css'

document.getElementById('inputKnop').addEventListener('click', () => {
  const text = funcs.convertToTitleCase(document.getElementById('input').value)
  document.getElementById('output').innerHTML = text
})
